/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.jozijugresting;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import org.openrap.jozijugapi.JoziJugService;

/**
 * REST Web Service
 *
 * @author richard
 */
@Stateless
@Path("generic")
public class GenericResource {

    @Resource
    private JoziJugService service;
    
    @Context
    private UriInfo context;

    /** Creates a new instance of GenericResource */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of org.openrap.jozijugresting.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/plain")
    public String getText() {
       return service.ping("Marius");
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/plain")
    public void putText(String content) {
    }
}
