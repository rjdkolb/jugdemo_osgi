package org.openrap.jozijugserviceimpl;

import org.openrap.jozijugapi.JoziJugService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    public void start(BundleContext context) throws Exception {
        System.out.println("HelloActivator::start");
        context.registerService(JoziJugService.class.getName(), new JoziJugServiceImpl(), null);
        //context.
        System.out.println("HelloActivator::registration of Hello service successful");  
    }

    public void stop(BundleContext context) throws Exception {
       context.ungetService(context.getServiceReference(JoziJugService.class.getName()));
        System.out.println("HelloActivator stopped");
    }

}
